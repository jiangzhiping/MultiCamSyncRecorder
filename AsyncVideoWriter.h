//
//  AsyncVideoWriter.h
//  MultiCamLogger
//
//  Created by 蒋志平 on 2017/12/1.
//

#ifndef AsyncVideoWriter_h
#define AsyncVideoWriter_h

#include <deque>
#include <chrono>
#include <thread>
#include <opencv2/opencv.hpp>
#include <mutex>
#include "Date2StringConversion.h"

class AsyncVideoWriter {
    
public:
    int outputFPS = 15;
    std::string videoCodec = "mp4v";
    std::chrono::nanoseconds segmentDuration = std::chrono::minutes(1);
    std::string filePrefix = "FilePrefixNotSpecified_";
    double zoomRatio = 1.0;
    
    AsyncVideoWriter (){
        auto writerThread = std::thread(&AsyncVideoWriter::videoWriterDaemonTask, this);
        writerThread.detach();
    }

    /**
     write frame, asynchronously.

     @param videoFrame cv::Mat frame
     */
    void writeFrame(const cv::Mat & videoFrame) {
        std::lock_guard<std::mutex> lock(writeMutex);
        frameTimeBuffer.emplace_back(std::chrono::steady_clock::now());
        if (zoomRatio == 1.0)
            frameBuffer.emplace_back(videoFrame);
        else {
            cv::Mat zoomedFrame;
            cv::resize(videoFrame, zoomedFrame, cv::Size(), zoomRatio, zoomRatio);
            frameBuffer.emplace_back(zoomedFrame);
        }
        
        if(frameBuffer.size() > bufferLength) {
            frameBuffer.pop_front();
            frameTimeBuffer.pop_front();
        }
        rxCV.notify_one();
    }

    void stopWriting() {
        std::lock_guard<std::mutex> lock(writeMutex);
        daemonWorking = false;
        videoWriter->release();
    }
    
private:
    std::chrono::steady_clock::time_point startTime;
    std::string videoFileExtension = ".avi";
    std::string videoFileName;
    
    int bufferLength = 100;
    std::deque<cv::Mat> frameBuffer;
    std::deque<std::chrono::steady_clock::time_point> frameTimeBuffer;
    std::shared_ptr<cv::VideoWriter> videoWriter;
    
    std::mutex rxMutex;
    std::condition_variable rxCV;
    std::mutex writeMutex;
    bool daemonWorking = true;
    
    void videoWriterDaemonTask() {
        std::unique_lock<std::mutex> lock(rxMutex);
        while(daemonWorking) {
            if (frameBuffer.size() > 0 && frameTimeBuffer.size() > 0) {
                auto nowTime = frameTimeBuffer.front();
                if (nowTime - startTime > segmentDuration) {
                    initializeWriter(nowTime);
                }
                writerCoreTask();
            }
            rxCV.wait(lock, [&] ()->bool {
                return frameBuffer.size() > 0 && frameTimeBuffer.size() > 0;
            });
        }
    };
    
    void initializeWriter(std::chrono::steady_clock::time_point nowTime) {
        startTime = nowTime;
        auto nowString = Date2StringConversion::chronoTimePoint2String(startTime);
        videoFileName = this->filePrefix + nowString + videoFileExtension;
        
        if (videoWriter) {
            videoWriter->release();
            videoWriter = nullptr;
        }
        videoWriter = std::make_shared<cv::VideoWriter>();
    }
    
    void writerCoreTask() {
        std::lock_guard<std::mutex> lock(writeMutex);
        while(frameBuffer.size() > 0) {
            if (videoWriter->isOpened() == false) {
                auto fourcc = CV_FOURCC(videoCodec[0], videoCodec[1], videoCodec[2], videoCodec[3]);
                videoWriter->open(videoFileName, fourcc, outputFPS, cv::Size2i(frameBuffer.front().cols, frameBuffer.front().rows));
            }
            const auto & frame = frameBuffer.front();
            videoWriter->write(frame);
            frameBuffer.pop_front();
            frameTimeBuffer.pop_front();
        }
    }
};

#endif /* AsyncVideoWriter_h */
