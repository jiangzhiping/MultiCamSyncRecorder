//
// Created by root on 17-10-2.
//

#ifndef TRINITY_DATE2STRINGCONVERSION_H
#define TRINITY_DATE2STRINGCONVERSION_H

#include <string>
#include <sstream>
#include <chrono>
#include <time.h>
#include <iomanip>

/**
 * Utility class provides time class conversion.
 */
class Date2StringConversion {
public:
    static std::string chronoTimePoint2String(std::chrono::system_clock::time_point timePoint, std::string dateFormat = "%y%m%d_%H%M%S");
    static std::string chronoTimePoint2String(std::chrono::steady_clock::time_point timePoint, std::string dateFormat = "%y%m%d_%H%M%S");
    static std::string time_t2String(std::time_t timet, std::string dateFormat = "%y%m%d_%H%M%S");
};

#endif //TRINITY_DATE2STRINGCONVERSION_H
