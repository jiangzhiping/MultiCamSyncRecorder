//
//  ConnectedVideoCapture.hpp
//  BinoCare
//
//  Created by 蒋志平 on 2017/11/30.
//

#ifndef ConnectedVideoCapture_hpp
#define ConnectedVideoCapture_hpp

#include <deque>
#include <chrono>
#include <thread>
#include <condition_variable>
#include <opencv2/opencv.hpp>
#include <mutex>
#include <unordered_map>
#include <unistd.h>

/**
 Periodically delay #delay_us microseconds in a loop
 
 This method compensates the execution time in a loop

 @param delay_us delay time in microseconds
 @return 0 for success.
 */
int32_t delay_periodic(int32_t delay_us);

/**
 Wrap cv::VideoCapture object and automatically connected to all other ConnectedVideoCapture instance, so that we can perform multi-thread camera accessing.
 
 The usage has two-step:
 1. Wrap existing cv::VideoCapture object(s) using createInstance method. These ConnectedVideoCapture are internally connected.
 2. Access/retrieve frames from multiple(or local) VideoCapture simultaneously using groupRetrieveSync(or retrieveSync) method.
 
 */
class ConnectedVideoCapture {

public:
    int desiredFPS = INT32_MAX;
    
    /**
     Create an instance of ConnectedVideoCapture.
     
     This method create an cv::VideoCapture instance internally and calls createInstance.

     @param deviceId OpenCV defefined cameraId
     @return a ConnectedVideoCapture instance wrapping around the input VideoCapture instance.
     
     @see createInstance
     */
    static std::shared_ptr<ConnectedVideoCapture> createInstance(int deviceId) {
        auto * camInstance = new cv::VideoCapture(deviceId);
        return createInstance(camInstance);
    }
    
    /**
     Create an instance of ConnectedVideoCapture.
     
     Error occurs if the VideoCapture instance is not opened.
     
     This method will register the created instance to a static array.

     @param videoCapturePtr a pointer of an already-opened cv::VideoCapture instance
     @return a ConnectedVideoCapture instance wrapping around the input VideoCapture instance.
     */
    static std::shared_ptr<ConnectedVideoCapture> createInstance(cv::VideoCapture * videoCapturePtr) {
        auto instance = std::shared_ptr<ConnectedVideoCapture>(new ConnectedVideoCapture(videoCapturePtr));
        connectedCameras.emplace_back(instance);
        return instance;
    }
    
    /**
     Retrieve new frames using the wrapped/local VideoCapture.
     
     This method will retrieve #targetFrameNumber new frames using the underpinning VideoCapture instance.
     This method will block the working thread until #targetFrameNumber frames are retrieved.
     
     @param targetFrameNumber number of frames to be retrieved, default is 1.
     @return deque of retrieved frames
     */
    std::deque<cv::Mat> retrieveSync(int targetFrameNumber = 1) {
        std::unique_lock<std::mutex> lock(rxMutex);
        retrieveAsync(targetFrameNumber);
        rxCV.wait(lock, [&] ()->bool {
            return frameBuffer.size() == targetFrameNumber;
        });
        
        return frameBuffer;
    }

    void stopRetrieving() {
        daemonWorking = false;
        videoCapture->release();
    }
    
    /**
     Retrieve new frames from ALL ConnectedVideoCapture(s), regardless of who calls this method.
     
     The retrieval action is implemented in multi-threaded way, but the retrieved frames are stored in the sequence of registration, e.g, newly retrieved #targetFrameNumber frames from 2nd VideoCapture is stored behind the same of the 1st VideoCapture. For the very frames retrieved from the same camera, @see retrieveSync

     @param targetFrameNumber number of frames to be retrieved, default is 1.
     @return deque of retrieved frames
     */
    std::deque<cv::Mat> groupRetrieveSync(int targetFrameNumber = 1) {
        std::unique_lock<std::mutex> lock(groupRxMutex);
        groupRetrieveAsync(targetFrameNumber);
        groupRxCV.wait(lock, [&] ()->bool {
            bool allDone = true;
            for(const auto & camera: connectedCameras) {
                if (camera->frameBuffer.size() != targetFrameNumber) {
                    allDone = false;
                    break;
                }
            }
            return allDone;
        });
        
        std::deque<cv::Mat> resultQueue;
        for(const auto & camera: connectedCameras) {
            for(const auto & frame: camera->frameBuffer)
                resultQueue.emplace_back(frame);
        }
        
        return resultQueue;
    }
    
    
    /**
     Perform groupRetrieveSync and concatnate the frames from different VideoCapture(s) into one frame.

     @param targetFrameNumber number of frames to be retrieved, default is 1
     @param horizontal Concatnation orientation, true for horizontal, false for vertical, default is horizontal.
     @return deque of concatnated frames
     */
    std::deque<cv::Mat> groupRetrieveSync_Concat(int targetFrameNumber = 1, bool horizontal = true) {
        auto groupRetrievedFrames = groupRetrieveSync(targetFrameNumber);
        auto deviceNumber = connectedCameras.size();
        
        std::deque<cv::Mat> resultQueue;
        for(auto i = 0 ; i < targetFrameNumber; i ++) {
            auto mergedWidth = 0, mergedHeight = 0, maxWidth = 0, maxHeight = 0;
            for(auto j = 0 ; j < deviceNumber ; j ++) {
                const auto & frame = groupRetrievedFrames[j * targetFrameNumber + i];
                mergedWidth += frame.cols;
                mergedHeight += frame.rows;
                maxWidth  = frame.cols > maxWidth ? frame.cols : maxWidth;
                maxHeight = frame.rows > maxHeight ? frame.rows : maxHeight;
            }
            cv::Point2i currentTopLeft(0,0);
            if (horizontal) {
                cv::Mat3b mergedFrame = cv::Mat3b::zeros(maxHeight, mergedWidth);
                for(auto j = 0 ; j < deviceNumber ; j ++) {
                    const auto & frame = groupRetrievedFrames[j * targetFrameNumber + i];
                    frame.copyTo(mergedFrame.colRange(currentTopLeft.x, currentTopLeft.x + frame.cols).rowRange(0, frame.rows));
                    currentTopLeft.x += frame.cols;
                }
                resultQueue.emplace_back(mergedFrame);
            } else {
                cv::Mat3b mergedFrame = cv::Mat3b::zeros(mergedHeight, maxWidth);
                for(auto j = 0 ; j < deviceNumber ; j ++) {
                    const auto & frame = groupRetrievedFrames[j * targetFrameNumber + i];
                    frame.copyTo(mergedFrame.rowRange(currentTopLeft.y, currentTopLeft.y + frame.rows).colRange(0, frame.cols));
                    currentTopLeft.y += frame.rows;
                }
                resultQueue.emplace_back(mergedFrame);
            }
        }
        return resultQueue;
    }

    void groupStopRetrieving() {
        for(const auto & camera: connectedCameras) {
            camera->stopRetrieving();
        }
    }
    
    
private:
    static std::deque<std::shared_ptr<ConnectedVideoCapture>> connectedCameras;
    static std::mutex groupRxMutex;
    static std::condition_variable groupRxCV;
    
    std::shared_ptr<cv::VideoCapture> videoCapture;
    std::deque<cv::Mat> frameBuffer;
    int captureBudge = 0;
    int bufferLength = 5;
    
    std::mutex budgeMutex;
    std::condition_variable budgeCV;
    std::mutex rxMutex;
    std::condition_variable rxCV;
    bool daemonWorking = true;
    
    
    ConnectedVideoCapture(cv::VideoCapture * videoCapture): videoCapture(videoCapture) {
        assert(this->videoCapture && this->videoCapture->isOpened());
        auto captureLoop = [this]() {
            std::unique_lock<std::mutex> lock(budgeMutex);
            while(daemonWorking) {
                while(captureBudge > 0) {
                    delay();
                    this->captureTask();
                    captureBudge -= 1;
                }
                budgeCV.wait(lock, [this]()->bool {
                    return captureBudge > 0;
                });
            }
        };
        
        std::thread captureThread(captureLoop);
        captureThread.detach();
    }
    
    inline void captureTask() {
        static cv::Mat retrievedFrame;
        if (videoCapture) {
            videoCapture->read(retrievedFrame);
            frameBuffer.emplace_back(retrievedFrame.clone());
            if(frameBuffer.size() > bufferLength)
                frameBuffer.pop_front();
            rxCV.notify_one();
            groupRxCV.notify_one();
        }
    }
    
    inline void delay() {
        if (desiredFPS == INT32_MAX)
            return;
        
        auto delay_us = 1.0e6 / desiredFPS;
        delay_periodic(delay_us);
    }
    
    void retrieveAsync(int targetFrameNumber = 1) {
        bufferLength = targetFrameNumber + 1;
        if (videoCapture) {
            frameBuffer.clear();
            captureBudge = targetFrameNumber;
            budgeCV.notify_one();
        }
    }
    
    void groupRetrieveAsync(int targetFrameNumber = 1) {
        for(const auto & camera: connectedCameras) {
            camera->retrieveAsync(targetFrameNumber);
        }
    }
};

int32_t delay_periodic(int32_t delay_us) {
    static thread_local struct timespec lastTime, nowTime;
    if (delay_us) {
        clock_gettime(CLOCK_MONOTONIC, &nowTime);
        int32_t timeDiff = (nowTime.tv_sec - lastTime.tv_sec) * 1000000 +
        (nowTime.tv_nsec - lastTime.tv_nsec + 500) / 1000;
        timeDiff = delay_us - timeDiff;
        if (timeDiff > 0 && timeDiff < delay_us)
            usleep(timeDiff);
        clock_gettime(CLOCK_MONOTONIC, &lastTime);
    }
    return 0;
}

std::deque<std::shared_ptr<ConnectedVideoCapture>> ConnectedVideoCapture::connectedCameras;
std::mutex ConnectedVideoCapture::groupRxMutex;
std::condition_variable ConnectedVideoCapture::groupRxCV;

#endif /* ConnectedVideoCapture_hpp */
