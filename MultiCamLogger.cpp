//
//  MultiCamLogger.cpp
//  MultiCamLogger
//
//  Created by 蒋志平 on 2017/11/30.
//

#include <signal.h>
#include <boost/program_options.hpp>
#include <boost/algorithm/string.hpp>
#include "ConnectedVideoCapture.hpp"
#include "AsyncVideoWriter.h"

namespace po = boost::program_options;

// Video Capture Facility
std::vector<std::shared_ptr<ConnectedVideoCapture>> connectedInstances;
AsyncVideoWriter writer;

std::vector<int> camIds = {0}; // fill in the camera id(s).
auto desiredFPS = 15; // capture and writer FPS
bool catHorizontally = true;
auto segmentLength_sec = 60; // length of each video file, in seconds
std::string filePrefix = "MultiCam"; // file prefix of the video
auto outputZoomRatio = 1.0; // zoom ratio for the output, 1.0 is original
std::string writerCodec = "mp4v"; // video codec, mp4v is efficent on macOS. Linux is unknown.
bool showImage = false;

void processProgramOptions(int argc, char** argv) {
    auto options = std::make_shared<po::options_description>("Supported Options by MultiCam VideoWriter");
    options->add_options()
            ("device-id", po::value<std::vector<int>>()->multitoken(), "device ID, can be multiple, separated by space. [0, 1, ...., default is 0]")
            ("fps", po::value<int>(), "video capturing and writing FPS, NOTE: this value is not guaranteed! When it exceeds the highest supported capturing FPS, the dumped video may not be time-aligned. [default is 15]")
            ("cat-orientation", po::value<std::string>(), "concatenation orientation [horizontal | vertical, default is horizontal]")
            ("video-segment-length", po::value<int>(), "length (in second) for video auto-segmentation, [0 for infinite]")
            ("video-file-prefix", po::value<std::string>(), "prefix string for video output file. [default is MultiCam]")
            ("zoom-ratio", po::value<double>(), "zoom ratio for output video. [0.0 ~ 1.0, default is 1.0]")
            ("codec", po::value<std::string>(), "video codec string for video dumping. [four_cc supported value, default is mp4v]")
            ("show", "show dumped video image")
            ("help", "print Description and Usage");

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, *options), vm);
    po::notify(vm);

    if (vm.count("device-id")) {
        auto ids = vm["device-id"].as<std::vector<int>>();
        camIds = ids;
    }

    if (vm.count("fps")) {
        auto fps = vm["fps"].as<int>();
        assert(fps > 0);
        desiredFPS = fps;
    }

    if (vm.count("cat-orientation")) {
        auto orientation = vm["cat-orientation"].as<std::string>();
        if (boost::iequals(orientation, "horizontal")) {
            catHorizontally = true;
        } else if (boost::iequals(orientation, "vertical")) {
            catHorizontally = false;
        } else {
            throw std::runtime_error("wrong parameter for option cat-orientation");
        }
    }

    if (vm.count("video-segment-length")) {
        auto segmentLength = vm["video-segment-length"].as<int>();
        assert(segmentLength >= 0);
        segmentLength_sec = segmentLength;
    }

    if (vm.count("video-file-prefix")) {
        auto prefix = vm["video-file-prefix"].as<std::string>();
        filePrefix = prefix;
    }

    if (vm.count("zoom-ratio")) {
        auto zoomRatio = vm["zoom-ratio"].as<double>();
        assert(zoomRatio > 0.0);
        outputZoomRatio = zoomRatio;
    }

    if (vm.count("codec")) {
        auto codec = vm["codec"].as<std::string>();
        writerCodec = codec;
    }

    if (vm.count("show")) {
        showImage = true;
    }

    if (vm.count("help")) {
        std::cout<<*options<<std::endl;
        exit(0);
    }

    if (vm.size() == 0) {
        std::cout<<*options<<std::endl;
        exit(0);
    }
}

void quit_handle(int signum);

int main(int argc, char** argv) {

    signal(SIGINT, quit_handle);
    processProgramOptions(argc, argv);

    for(const auto & camId: camIds) {
        auto instance = ConnectedVideoCapture::createInstance(camId);
        instance->desiredFPS = desiredFPS;
        connectedInstances.emplace_back(instance);
    }

    writer.outputFPS = desiredFPS;
    writer.segmentDuration = std::chrono::seconds(segmentLength_sec);
    writer.filePrefix = filePrefix;
    writer.videoCodec = writerCodec;
    writer.zoomRatio = outputZoomRatio;
    
    // Do capturing and writing
    while(true) {
        // This one-line magic will perform multi-thread camera accessing for both cam0 and cam1, and concatnate the retrived frames horizontally.
        auto retrievedFrames = connectedInstances[0]->groupRetrieveSync_Concat(1, catHorizontally);
        // This line will write the retrieved frame to disk, asynchronoously.
        writer.writeFrame(retrievedFrames[0]);
        if(showImage) {
            cv::imshow("MultiCam Logger [press q to quit safely]", retrievedFrames[0]);
            auto key = cv::waitKey(1);
            if (key == 'q') {
                break;
            }
        }
    }

    quit_handle(0);
}


void quit_handle(int signum)
{
    connectedInstances[0]->groupStopRetrieving();
    writer.stopWriting();
    exit(signum);
}