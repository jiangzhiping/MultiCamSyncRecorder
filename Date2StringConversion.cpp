//
// Created by root on 17-10-2.
//

#include "Date2StringConversion.h"

std::string Date2StringConversion::time_t2String(std::time_t timet, std::string dateFormat) {
    std::stringstream ss;
    ss << std::put_time(std::localtime(&timet), dateFormat.c_str());
    return ss.str();
}

std::string
Date2StringConversion::chronoTimePoint2String(std::chrono::system_clock::time_point timePoint, std::string dateFormat) {
    return time_t2String(std::chrono::system_clock::to_time_t(timePoint));
}

std::string
Date2StringConversion::chronoTimePoint2String(std::chrono::steady_clock::time_point timePoint, std::string dateFormat) {
    auto epoch_diff = std::chrono::system_clock::now().time_since_epoch().count() - std::chrono::steady_clock::now().time_since_epoch().count();
    std::chrono::system_clock::time_point system_clock{std::chrono::system_clock::duration(timePoint.time_since_epoch().count() + epoch_diff)};
    return chronoTimePoint2String(system_clock, dateFormat);
}
