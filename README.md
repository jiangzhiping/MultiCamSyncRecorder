# MultiCamSyncRecorder

## What It Does?
This small console application does 3 things quite well:
* Capture video frames from multiple cameras synchronously via multi-thread capturing;
* Concatenate the frames into one single frames(Horizontal or Vertical)
* Write the merged big frame into video file.
* [Optional] Write to next video file when a specified timer has passed.

## How to Use ?

run with "--help" option to see the usage.

## How To Get It?

You have two way to get MultiCamSyncRecorder:

* If running on ubuntu system, you can [download .deb installer](https://gitlab.com/jiangzhiping/MultiCamSyncRecorder/-/jobs/artifacts/master/download?job=build) built by GitLab CI (Continous Integration)
* Or, checkout the source code and build your own.

## Checkout Source Code And Build Your Own
Run following command in terminal:
```
sudo apt-get update && apt-get -y install cmake build-essential libboost-all-dev libopencv-dev
cd ~
git https clone://gitlab.com/jiangzhiping/MultiCamSyncRecorder.git
cd MultiCamSyncRecorder
mkdir build
cd build
cmake ..
make package
cd ..
```

If everything goes fine, you will see file "MultiCamSyncRecorder-VERSION-Linux.deb", which is the packaged installer built by your own.
Double-click this file to start installation.

## Cross-Platform ?
Tested and verified on Ubuntu 16.04, Ubuntu 17.10 and macOS 10.13.
Windows, Should work.


